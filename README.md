# `ee`

`ee` is a tool similar to `dd` but written with io-uring

## Installation

You can install the latest version from the git repository:

```bash
cargo install --git https://gitlab.com/dzamlo/ee.git
```

If you use Arch Linux, a `PKGBUILD` file is present in the `aur` directory.

bash, zsh and fish completion files are also generated but not installed with `cargo install`. To use them you need to either use the `PKGBUILD` file, or manually build the application yourself and copy the files.

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
