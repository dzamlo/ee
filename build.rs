extern crate clap;
extern crate clap_complete;

use clap::CommandFactory;
use clap_complete::{generate_to, Shell};
use std::io::Error;

include!("src/cli.rs");

fn main() -> Result<(), Error> {
    let mut cmd = Args::command();
    let out_dir = std::env::var_os("OUT_DIR").unwrap();
    let path = generate_to(Shell::Bash, &mut cmd, "ee", &out_dir)?;
    println!("cargo:warning=bash completion file is generated: {path:?}");

    let path = generate_to(Shell::Fish, &mut cmd, "ee", &out_dir)?;
    println!("cargo:warning=fish completion file is generated: {path:?}");

    let path = generate_to(Shell::Zsh, &mut cmd, "ee", &out_dir)?;
    println!("cargo:warning=zsh completion file is generated: {path:?}");

    Ok(())
}
