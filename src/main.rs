#![allow(clippy::needless_range_loop)]
use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::os::fd::AsRawFd;

use clap::Parser;
use io_uring::types::Fixed;
use io_uring::IoUring;
use libc::iovec;

use crate::cli::Args;

mod cli;
fn main() -> io::Result<()> {
    let args = Args::parse();

    let conversions = args.conv.unwrap_or_default();

    let input_file = if let Some(ref path) = args.input_file {
        Some(File::open(path)?)
    } else {
        None
    };

    let input_fd = input_file
        .as_ref()
        .map(|f| f.as_raw_fd())
        .unwrap_or_else(|| io::stdin().as_raw_fd());

    let output_file = if let Some(ref path) = args.output_file {
        let mut open_options = OpenOptions::new();
        open_options
            .write(true)
            .truncate(args.truncate)
            .append(!args.truncate)
            .create(!args.no_create);
        Some(open_options.open(path)?)
    } else {
        None
    };

    let output_fd = output_file
        .as_ref()
        .map(|f| f.as_raw_fd())
        .unwrap_or_else(|| io::stdout().as_raw_fd());

    let mut ring = IoUring::builder()
        .setup_sqpoll(2000)
        .build(args.ring_entries as u32)?;

    ring.submitter().register_files(&[input_fd, output_fd])?;
    const INPUT_FIXED: Fixed = Fixed(0);
    const OUTPUT_FIXED: Fixed = Fixed(1);

    let mut bufs = Vec::with_capacity(args.ring_entries as _);
    let mut bufs_iovecs = Vec::with_capacity(args.ring_entries as _);

    for i in 0..args.ring_entries as usize {
        bufs.push(vec![0u8; args.block_size as usize]);
        bufs_iovecs.push(iovec {
            iov_base: bufs[i].as_mut_ptr() as _,
            iov_len: bufs[i].len(),
        })
    }

    ring.submitter().register_buffers(&bufs_iovecs)?;

    let mut remaining_reads_to_submit = args.count;
    let mut remaining_writes_to_submit = args.count;
    let mut remaining_entries_to_receive = 0;

    // If count is < than the number of ring entries, we don't need to fill the ring, but only insert count entries
    // There is no overflow possible when converting from u128 to usize, because the largest possible value is if ring eng_entries is 2^16-1 (it's an u16)
    for i in 0..(args.ring_entries as u128).min(args.count) as usize {
        let read_e = io_uring::opcode::ReadFixed::new(
            INPUT_FIXED,
            bufs[i].as_mut_ptr(),
            bufs[i].len() as _,
            i as _,
        )
        .offset(-1)
        .build()
        .user_data(i as _)
        .flags(io_uring::squeue::Flags::IO_DRAIN);

        unsafe {
            ring.submission()
                .push(&read_e)
                .expect("submission queue is full, should not be possible");
        }
        remaining_reads_to_submit -= 1;
        remaining_entries_to_receive += 1;
    }

    ring.submit_and_wait(1)?;
    remaining_entries_to_receive -= 1;

    const IS_WRITE: u64 = 1 << 17;
    while remaining_reads_to_submit > 0 || remaining_writes_to_submit > 0 {
        let cqe = wait_for_cqe(&mut ring, &mut remaining_entries_to_receive)?;

        let result = cqe.result();
        let user_data = cqe.user_data();
        let is_write = (user_data & IS_WRITE) != 0;
        let buffer_num = (user_data & u16::MAX as u64) as usize;
        assert!(result >= 0, "read or write error: {result}");

        if !is_write && result == 0 {
            break;
        }

        if !is_write {
            for conversion in &conversions {
                conversion.convert_slice(&mut bufs[buffer_num][0..result as usize])
            }
            let write_e = io_uring::opcode::WriteFixed::new(
                OUTPUT_FIXED,
                bufs[buffer_num].as_mut_ptr(),
                result as u32,
                buffer_num as u16,
            )
            .offset(-1)
            .build()
            .user_data(buffer_num as u64 | IS_WRITE)
            .flags(io_uring::squeue::Flags::IO_DRAIN);
            unsafe {
                ring.submission()
                    .push(&write_e)
                    .expect("submission queue is full, should not be possible");
            }
            remaining_writes_to_submit -= 1;
            remaining_entries_to_receive += 1;
        } else if remaining_reads_to_submit > 0 {
            let read_e = io_uring::opcode::ReadFixed::new(
                INPUT_FIXED,
                bufs[buffer_num].as_mut_ptr(),
                args.block_size,
                buffer_num as u16,
            )
            .offset(-1)
            .build()
            .user_data(buffer_num as u64)
            .flags(io_uring::squeue::Flags::IO_DRAIN);
            unsafe {
                ring.submission()
                    .push(&read_e)
                    .expect("submission queue is full, should not be possible");
            }
            remaining_entries_to_receive += 1;
            remaining_reads_to_submit -= 1;
        }
    }

    while remaining_entries_to_receive > 0 {
        let cqe = wait_for_cqe(&mut ring, &mut remaining_entries_to_receive)?;

        let result = cqe.result();
        assert!(result >= 0, "read or write error: {result}");
    }

    Ok(())
}

fn wait_for_cqe(
    ring: &mut IoUring,
    remaining_entries_to_receive: &mut i32,
) -> io::Result<io_uring::cqueue::Entry> {
    if ring.completion().is_empty() {
        ring.submit_and_wait(1)?;
    }
    let cqe = ring
        .completion()
        .next()
        .expect("completion queue is empty, should not be possible");
    *remaining_entries_to_receive -= 1;
    Ok(cqe)
}
