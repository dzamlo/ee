use std::path::PathBuf;

use clap::Parser;
use clap::ValueEnum;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Input file, default to stdin
    #[arg(short, long)]
    pub input_file: Option<PathBuf>,

    /// Output file, default to stdout
    #[arg(short, long)]
    pub output_file: Option<PathBuf>,

    /// Block size in bytes, same for reading and writing
    #[arg(short, long, default_value_t = 512)]
    pub block_size: u32,

    /// Number of blocks to read and write, may stop earlier in case of EOF of the input
    #[arg(short, long, default_value_t = u128::MAX)]
    pub count: u128,

    /// Number of entries in the io-uring ring buffers, should be a power of two
    #[arg(short, long, default_value_t = 8)]
    pub ring_entries: u16,

    /// Truncate the output file instead of appending to it
    #[arg(long)]
    pub truncate: bool,

    /// Don't create the output file if it doesn't exist
    #[arg(long)]
    pub no_create: bool,

    /// Convert the data before writing it
    #[arg(long, value_enum)]
    pub conv: Option<Vec<Conv>>,
}

#[derive(Copy, Clone, Debug, ValueEnum)]
pub enum Conv {
    AsciiLower,
    AsciiUpper,
    AsciiCaseInvert,
    BinaryInvert,
}

impl Conv {
    pub fn convert_slice(self, slice: &mut [u8]) {
        match self {
            Conv::AsciiLower => slice.make_ascii_lowercase(),
            Conv::AsciiUpper => slice.make_ascii_uppercase(),
            Conv::AsciiCaseInvert => {
                for byte in slice {
                    if byte.is_ascii_uppercase() {
                        byte.make_ascii_lowercase()
                    } else if byte.is_ascii_lowercase() {
                        byte.make_ascii_uppercase()
                    }
                }
            }
            Conv::BinaryInvert => {
                for byte in slice {
                    *byte = !*byte;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::cli::Conv;

    #[test]
    fn convert_slice_ascii_lower_works() {
        let mut input = [0, 0xFF, 'A' as u8, 'c' as u8, 'Z' as u8];
        let expected = [0, 0xFF, 'a' as u8, 'c' as u8, 'z' as u8];
        Conv::AsciiLower.convert_slice(&mut input);
        assert_eq!(input, expected);
    }

    #[test]
    fn convert_slice_ascii_upper_works() {
        let mut input = [0, 0xFF, 'a' as u8, 'C' as u8, 'z' as u8];
        let expected = [0, 0xFF, 'A' as u8, 'C' as u8, 'Z' as u8];
        Conv::AsciiUpper.convert_slice(&mut input);
        assert_eq!(input, expected);
    }

    #[test]
    fn convert_slice_ascii_case_invert_works() {
        let mut input = [0, 0xFF, 'A' as u8, 'c' as u8, 'Z' as u8];
        let expected = [0, 0xFF, 'a' as u8, 'C' as u8, 'z' as u8];
        Conv::AsciiCaseInvert.convert_slice(&mut input);
        assert_eq!(input, expected);
    }

    #[test]
    fn convert_slice_binary_invert_works() {
        let mut input = [0, 0xFF, 0xFE, 1];
        let expected = [0xFF, 0, 1, 0xFE];
        Conv::BinaryInvert.convert_slice(&mut input);
        assert_eq!(input, expected);
    }
}
